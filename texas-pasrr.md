# Texas PASRR Overview for Nursing Facilities
## Updated June 2014

---

## Who Am I?
  - Mark Adams
  - Sr. Software Engineer at SimpleLTC
  - mark@simpleltc.com
  - Lead Developer for SimpleLTC's PASRR implementation

---

## Background
* SimpleLTC software simplifies healthcare information for long-term care
* In Texas, many know us as the makers of SimpleCFS™ the leading alternative for
  managing Medicaid forms and MESAV data
* For more information about SimpleLTC, visit http://www.simpleltc.com

---

# What Is PASRR?
PASRR is required part of each state's Medicaid program to ensure
that those with MI/IDD are cared for properly

* **P**re-
* **A**dmission
* **S**creening and
* **R**esident
* **R**eview

---

## Three Core Parts
* **Level I Screening (TX: PL1)**
    * Is there a chance the individual might have MI/IDD?
* **Level II Evaluation (TX: PE)**
    * Do they have MI/IDD?
    * What specialized services do they need?
    * What is the right setting for that care?
* **Resident Review**
    * When does a resident need to be reevaluated?

---

# Texas PASRR Redesign
## A brief history
  * **1987**: Congress institutes PASRR so that residents with MI/IDD receive
  proper care in the proper setting
  * **Dec 2009**: CMS informs TX HHSC of Federal compliance issues with the TX PASRR program
  * **May 2013**: TX HHSC launches Phase I of PASRR redesign
  * **Jun 2014**: TX HHSC launches Phase II of PASRR redesign

---

## CMS Identified Three Main Problems
  1. Level II evaluation needs to recommend specialized services prior to admission
  2. NF staff should not perform the Level II evaluation
  3. State needs to describe resident review process better

---

## Redesign Phase I
* Rolled out May 2013
* PL1 must be completed for every individual being admitted to a Medicaid NF
    * Preadmission: RE completes PL1. If positive, LA submits. Otherwise, NF submits.
    * Expedited Admission: RE completes and NF submits PL1
    * Exempt Hospital Discharge: RE completes and NF submits PL1

---

## Redesign Phase I
* Electronic message sent to LA to come and perform the PE (typically within 7 days)
    * Exception: Coma, Exempt Hospital Discharge, Respite (14 days)
* After PE, NF must certify ability to provide specialized services from PE
* LA coordinates placement in NF or alternate setting and participates in NF IDT

---

## Redesign Phase II
* Rolled out June 2014
* **LTCMI rejections and Medical Necessity**
    * If a PL1 is not on file for the resident, the LTCMI will be rejected
    * In other words, **No PL1 = No LTCMI = No Payment**
    * For preadmission PL1-positive individuals:
        * The LA will need to submit the PL1
        * MN will be determined for the first LTCMI based on the PE. Therefore,
        the LTCMI will not be accepted until the PE has been submitted by the LA.

---

## Redesign Phase II
* **New Alerts**
    * On LTCMI submission, if a matching PE is found but the PL1 has not been
      certified (D0100N), the NF receives an electronic alert telling them that
      they need to review the PE and certify their ability to serve.
        * Alerts can be viewed through SimpleLTC (for SimpleLTC customers) or
          the TMHP LTC Online Portal
    * On MDS, LA will automatically be notified to perform a PE if MDS coding shows
      potential PASRR eligibility
    * On MDS SCSA, if resident is no longer comatose, LA will automatically be
      notified to perform a PE

---

## Redesign Phase II
* **Validation Changes**
  * TMHP will now allow NFs to 'Update Form' on "data convert" PL1s
  * TMHP will now allow NFs to submit a PL1 after an individual has been admitted
    * D0100P NF Date of Entry can now be prior to the date the PL1 is submitted
    * D0100P NF Date of Entry can be prior to A1200B (Referring Entity Signature Date)
    * D0100P NF Date of Entry can be prior to A0600 (Date of Assessment)

---

## Redesign Phase II
* **CHOW Process**
    * Notify DADS PASRR Unit via e-mail (pasrr@dads.state.tx.us) that a CHOW is happening:
        1. Include Former NF Name, Address, Vendor/Contract, New NF Name, Date CHOW initiated, # of residents
        2. Keep a list of all residents admitted after the CHOW was started
        3. Once the new Contract # is active, a PL1 must be submitted for *every* resident within 90 days
    * DADS PASRR unit will contact the NF weekly to get a list of all newly admitted residents
    * After 90 days, DADS PASRR will verify a PL1 has been introduced for every resident pre- and post-CHOW

---

## Regulatory Notes
* **IDT/Care Plan Coordination**
    * NFs are responsible for coordinating with the LA to schedule the IDT /
      care plan meeting
        * Must be held within the first 14 days after admission
        * NF must inform the LA of the date and time of the meeting
        * Can be conducted over the phone
* **Delivery of Specialized Services**
    * Specialized services (by NF or LA) must be included the comprehensive care plan
    * All specialized services must be started within 30 days after being added
      to the care plan

---

## Regulatory Notes
* **Survey Tag F285**
    * If sampled residents have MI or ID, did the state determine:
        * Whether the residents needed the services of an NF?
        * Whether the residents need specialized services for their ID or MI?
    * This is fulfilled by the completion of the PE
        * Performed by the LA on behalf of the state
    * Facilities will have an issue during surveys if a resident has MI/ID but does not have a positive PE

---

## What is SimpleLTC doing to help?
* **Alerts**
    * Alerts from TMHP will be available within SimpleLTC
* **"Medicaid Residents Missing PL1" Report**
    * We've introduced a new report that identifies active residents (based on form history) who do not have a PL1 on file
* **Admission LTCMI Warning**
    * LTCMIs submitted through SimpleLTC will display a warning if the resident does not have a PL1 on file

---

# That's it!
**If you need more help...**

* SimpleLTC Customer Support
    * (469) 916-2803, support@simpleltc.com

* DADS PASRR Unit
    * (855) 435-7180, pasrr@dads.state.tx.us

---

# References
* [Federal Regulations - PASRR Program Requirements](http://www.law.cornell.edu/cfr/text/42/part-483/subpart-C)
    * 42 CFR 483.100-138
* [Federal Regulations - Resident Assessment (PASRR)](http://www.law.cornell.edu/cfr/text/42/483.20#m)
    * 42 CFR 483.20(m)
* [CMS State Operations Manual (100-07) - Appendix PP](http://cms.hhs.gov/Regulations-and-Guidance/Guidance/Manuals/downloads/som107ap_pp_guidelines_ltcf.pdf)
    *  Guidance to Surveyors for LTC Facilities
    *  Tag 285
* [Texas Administrative Code - PASRR](http://secure.sos.state.tx.us/pls/pub/readtac$ext.ViewTAC?tac_view=5&ti=40&pt=1&ch=17&sch=A&rl=Y)
    * 40 TAC §17
* [Texas DADS PASRR Website](www.dads.state.tx.us/providers/pasrr/)
